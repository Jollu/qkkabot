from dotenv import load_dotenv
import telebot
import os
import json


load_dotenv()
API_KEY = os.getenv("API_KEY")
MAINTAINER = os.getenv("MAINTAINER_CHAT_ID")

bot = telebot.TeleBot(API_KEY)
updates = bot.get_updates()

f = open('moderator_data.json')
moderators = json.load(f)
active = moderators['ids'][0]


@bot.message_handler(commands=['start', 'help'])
def welcome_message(message):
    bot.send_message(message.chat.id, u"Tervetuloa QKKACHATTIIN!")


@bot.message_handler(commands=['let_me_moderate'])
def request_mod(message):
    bot.send_message(MAINTAINER, f"@{message.from_user.username} haluaa moderoida, user_id: {message.chat.id}")


@bot.message_handler(commands=['moderoi'])
def set_me_active(message):
    global active
    chat_id = message.chat.id

    if chat_id not in moderators['ids']:
        bot.send_message(chat_id, "Et muuten ole moderaattori")
    else:
        bot.send_message(active, f"@{message.from_user.username} moderoi")
        active = chat_id
        bot.send_message(chat_id, "Hauskaa moderointia!!")


@bot.message_handler(content_types=['text'])
def copy_paste(message):
    bot.send_message(active, message.text)


bot.polling()
